# fabrica



## Установка

Выполните следующие команды.


```
git remote add origin https://gitlab.com/frollow/fabrica.git
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
cd sending_app
python manage.py runserver
```

http://127.0.0.1:8000/

http://127.0.0.1:8000/admin

login: demo 

password: demodemo

База данных для тестового задания была оставлена в проекте.
