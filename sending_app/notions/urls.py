from django.urls import path

from . import views

app_name = "notions"

urlpatterns = [
    path("", views.index, name="index"),
    path(
        "clients/form/",
        views.ClientCreateView.as_view(),
        name="clients_create_form",
    ),
    path(
        "clients/form/<int:pk>/edit/",
        views.ClientUpdateView.as_view(),
        name="clients_update_form",
    ),
    path(
        "clients/form/<int:pk>/delete/",
        views.ClientDeleteView.as_view(),
        name="clients_delete_form",
    ),
    path(
        "tasks/form/",
        views.NotionCreateTaskView.as_view(),
        name="tasks_create_form",
    ),
    path(
        "tasks/form/<int:pk>/edit/",
        views.NotionUpdateView.as_view(),
        name="tasks_update_form",
    ),
    path(
        "tasks/form/<int:pk>/delete/",
        views.NotionDeleteView.as_view(),
        name="tasks_delete_form",
    ),
    path("send/<int:pk>/", views.send_messages, name="send_messages"),
    path("tasks/<int:pk>/", views.notification_page, name="notification_page"),
]
