from django import template

register = template.Library()


@register.filter
def count_true(value):
    return value.filter(status=True).count()


@register.filter
def count_false(value):
    return value.filter(status=False).count()
