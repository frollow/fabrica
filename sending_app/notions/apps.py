from django.apps import AppConfig


class NotionsConfig(AppConfig):
    name = 'notions'
