import os
import requests

from django.utils import timezone
from dotenv import load_dotenv
from django.shortcuts import get_object_or_404, render, redirect
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from .forms import NotionTaskForm, ClientForm
from .models import NotionTask, Client, Messages

load_dotenv()
AUTH_TOKEN = os.getenv("AUTH_TOKEN")
URL = "https://probe.fbrq.cloud/v1/send/"


def index(request):
    notions = NotionTask.objects.order_by("-start_pub_date")[:10]
    clients = Client.objects.order_by(("-pk")[:10])
    now = timezone.now()
    context = {"notions": notions, "clients": clients, "now": now}
    return render(request, "notions/list.html", context)


def notification_page(request, pk):
    notion = get_object_or_404(NotionTask, pk=pk)
    context = {"notion": notion}
    return render(request, "notions/page.html", context)


def send_messages(request, pk):
    header = {"Authorization": "Bearer " + AUTH_TOKEN}
    notion = get_object_or_404(NotionTask, pk=pk)
    if notion.start_pub_date < timezone.now() < notion.end_pub_date:
        tags = notion.tags.all()
        clients = Client.objects.filter(tags__in=tags)

        def sending(client):
            number = int(client.operator_code + client.phone_number)
            data = {"id": client.id, "phone": number, "text": notion.text}
            response = requests.post(
                URL + str(data["id"]), json=data, headers=header
            )
            if response.status_code == 200:
                status = True
            Messages.objects.get_or_create(
                status=status,
                notion=notion,
                client=client,
            )

        for client in clients:
            try:
                sending(client)
            except requests.exceptions.HTTPError as errh:
                print("Http Error:", errh)
            except requests.exceptions.ConnectionError as errc:
                print("Error Connecting:", errc)
            except requests.exceptions.Timeout as errt:
                print("Timeout Error:", errt)
            except requests.exceptions.RequestException as err:
                print("OOps: Something Else", err)

    else:
        print("Time is over")
    return redirect("notions:index")


class NotionCreateTaskView(CreateView):
    form_class = NotionTaskForm
    template_name = "notions/form.html"
    success_url = "/"


class NotionUpdateView(UpdateView):
    model = NotionTask
    form_class = NotionTaskForm
    template_name = "notions/form_update.html"
    success_url = "/"


class NotionDeleteView(DeleteView):
    model = NotionTask
    template_name = "notions/form_delete.html"
    success_url = "/"


class ClientCreateView(CreateView):
    form_class = ClientForm
    template_name = "clients/form.html"
    success_url = "/"


class ClientUpdateView(UpdateView):
    model = Client
    form_class = ClientForm
    template_name = "clients/form_update.html"
    success_url = "/"


class ClientDeleteView(DeleteView):
    model = Client
    template_name = "clients/form_delete.html"
    success_url = "/"
