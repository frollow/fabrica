from django import forms
from .models import NotionTask, Client
from django.utils import timezone


class NotionTaskForm(forms.ModelForm):
    start_pub_date = forms.SplitDateTimeField(
        initial=timezone.now(),
    )
    end_pub_date = forms.SplitDateTimeField(initial=timezone.now())

    class Meta:
        model = NotionTask
        fields = "__all__"


class ClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = "__all__"
