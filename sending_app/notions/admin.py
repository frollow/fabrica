from django.contrib import admin

from .models import Client, NotionTask, Tag, Messages


class ClientAdmin(admin.ModelAdmin):
    list_display = ("pk", "operator_code", "phone_number", "time_zone")
    search_fields = (
        "phone_number",
        "tags",
    )
    list_filter = (
        "operator_code",
        "tags",
    )
    empty_value_display = "-пусто-"


admin.site.register(Client, ClientAdmin)


class NotionTaskAdmin(admin.ModelAdmin):
    list_display = (
        "pk",
        "text",
        "start_pub_date",
        "end_pub_date",
    )
    search_fields = (
        "start_pub_date",
        "end_pub_date",
        "tags",
    )
    list_filter = ("tags",)
    empty_value_display = "-пусто-"


admin.site.register(NotionTask, NotionTaskAdmin)


class TagAdmin(admin.ModelAdmin):
    list_display = (
        "pk",
        "name",
    )
    search_fields = ("name",)
    empty_value_display = "-пусто-"


admin.site.register(Tag, TagAdmin)


class MessagesAdmin(admin.ModelAdmin):
    list_display = (
        "pk",
        "notion",
        "client",
        "status",
    )
    empty_value_display = "-пусто-"


admin.site.register(Messages, MessagesAdmin)
