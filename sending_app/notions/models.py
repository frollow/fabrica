from django.db import models
from django.core.validators import RegexValidator


class Tag(models.Model):
    name = models.CharField(max_length=25)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Тег"
        verbose_name_plural = "Теги"


class NotionTask(models.Model):
    start_pub_date = models.DateTimeField(
        "Дата и время запуска рассылки", blank=True
    )
    end_pub_date = models.DateTimeField(
        "Дата и время окончания рассылки", blank=True
    )
    text = models.TextField(verbose_name="Текст сообщения", blank=True)
    tags = models.ManyToManyField(Tag, related_name="notion_task")

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки"


class Client(models.Model):
    phone_regex = RegexValidator(
        regex=r"^(7)?[0-9]{10}$",  # "/^([7]?[0-9]{3,11})*$/i;",
        message="Номер телефона должен быть в формате 7XXXXXXXXXX (X - цифра от 0 до 9)",
    )
    phone_number = models.CharField(
        validators=[phone_regex], max_length=11, blank=False
    )
    operator_code_regex = RegexValidator(
        regex=r"^[0-9]{1,5}$",
        message="Код мобильного оператора введен не верно. Должны быть только цифры",
    )
    operator_code = models.CharField(
        validators=[operator_code_regex], max_length=5, blank=False
    )
    tags = models.ManyToManyField(Tag, related_name="client")
    time_zone = models.SmallIntegerField(default=0, blank=True, null=True)

    def __str__(self):
        return self.phone_number

    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"


class Messages(models.Model):
    create_date = models.DateTimeField(
        "Дата создания (отправки)", auto_now_add=True, db_index=True
    )
    status = models.BooleanField(default=False)
    notion = models.ForeignKey(
        NotionTask,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="message",
    )
    client = models.ForeignKey(
        Client,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="message",
    )

    def __str__(self):
        return "messages type"

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"
