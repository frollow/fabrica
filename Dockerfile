FROM python:3.7-slim

RUN mkdir /app

COPY requirements.txt /app

RUN pip3 install -r /app/requirements.txt --no-cache-dir

COPY sending_app/ /app

WORKDIR /app

CMD ["python", "manage.py", "runserver", "0:8000"]

ENV AUTH_TOKEN eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NzQ3MjkxODYsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IkFydHlvbS5Gcm9sb3YifQ.3-OMtWkweSULenr2Xx7zn-npqyCpMd5PAX3bCugZlmE
